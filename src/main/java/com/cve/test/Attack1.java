package com.cve.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Attack1 {

    private static final Logger LOGGER = LogManager.getLogger(Attack1.class);


    /**
     * http://localhost:8080/attack1?logMsg=8765
     * @param logMsg
     * @return
     */
    @RequestMapping("/Attack1")
    public String sayHello(String logMsg) {
        LOGGER.error(logMsg);
        return "Hello World";
    }
}
