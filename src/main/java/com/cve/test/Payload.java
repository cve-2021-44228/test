package com.cve.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class Payload {

    /**
     * http://localhost:8080/attack1?logMsg=8765
     * @param logMsg
     * @return
     */
    @RequestMapping("/Payload")
    public String sayHello(String logMsg) {

        //直接在構造方法中運作電腦
        try {
            Runtime.getRuntime().exec("gnome-calculator");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Hello World";
    }
}
